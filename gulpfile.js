var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
var bowerDir = './resources/assets/bower/';

elixir(function(mix)
{
 //mix.sass('app.scss');

 mix.copy(bowerDir + '/bootstrap/fonts',               'public/build/fonts');
 mix.copy(bowerDir + '/components-font-awesome/fonts', 'public/build/fonts');
 mix.copy(bowerDir + '/Ionicons/fonts',                'public/build/fonts');

 mix.scripts([
  // For admin core
  'jquery/dist/jquery.min.js',
  'jquery-pjax/jquery.pjax.js',
  'jquery-ui/jquery-ui.min.js',
  'bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
  'bower-jvectormap/jquery-jvectormap-world-mill-en.js',
  'moment/min/moment.min.js',
  'bootstrap/dist/js/bootstrap.min.js',
  'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
  'bootstrap-daterangepicker/daterangepicker.js',
  'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js',
  'iCheck/icheck.min.js',
  'x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js',
  'jquery.sparkline.dist/dist/jquery.sparkline.min.js',
  'jQuery-Knob/dist/jquery.knob.min.js'

  // Ez máshoz??
  //'morris.js/morris.min.js',
  //'jquery-ujs/src/rails.js'
  //'../js/app.js'
 ], 'public/assets/admin.js', bowerDir);

 mix.styles([
  'bootstrap/dist/css/bootstrap.min.css',
  'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css',
  'bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
  'bootstrap-daterangepicker/daterangepicker.css',
  'bower-jvectormap/jquery-jvectormap-1.2.2.css',
  'components-font-awesome/css/font-awesome.min.css',
  'Ionicons/css/ionicons.min.css',
  //'morris.js/morris.css',
  'x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css'
 ], 'public/assets/admin.css', bowerDir);

 mix.version(['assets/admin.css', 'assets/admin.js']);

});



/**
 HTML minify
 **/
//var gulp = require('gulp');
//var minifyHTML = require('gulp-minify-html');

//gulp.task('compress', function() {
 //var opts = {
  //conditionals: true,
  //spare:true
 //};

 //return gulp.src('./storage/framework/views/**/*')
//     .pipe(minifyHTML(opts))
//     .pipe(gulp.dest('./storage/framework/views/'));
//});

//var htmlmin = require('gulp-htmlmin');
//var gulp = require('gulp');
//
//gulp.task('compress', function() {
//    var opts = {
//        collapseWhitespace:    true,
//        removeAttributeQuotes: true,
//        removeComments:        true,
//        minifyJS:              true
//    };
//
//    return gulp.src('./storage/framework/views/**/*')
//        .pipe(htmlmin(opts))
//        .pipe(gulp.dest('./storage/framework/views/'));
//});