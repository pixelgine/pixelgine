@extends('layouts.base')

@section('body')
    <div class="container">
        <div class="content">
            @yield('content')
        </div>
    </div>
@endsection
