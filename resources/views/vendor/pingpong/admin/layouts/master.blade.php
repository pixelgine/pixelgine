<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Administrator | @yield('title', 'Dashboard')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Base style -->
    <link rel="stylesheet" href="{{ URL::to('/') . elixir("assets/admin.css") }}">
    <!-- Theme style -->
    <link href="{!! admin_asset('adminlte/css/AdminLTE.css') !!}" rel="stylesheet" type="text/css"/>

    @yield('style')

    <!-- Base scripts -->
    <script src="{{ URL::to('/') . elixir("assets/admin.js") }}"></script>
    <!-- AdminLTE App -->
    <script src="{!! admin_asset('adminlte/js/AdminLTE/app.js') !!}" type="text/javascript"></script>
    <script src="{!! admin_asset('js/all.js') !!}" type="text/javascript"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <style>
        li.active > a {
            font-weight: bold;
        }
        .editable-container.popover {
            z-index: 99999;
        }
        .modal-dialog {
            z-index: 99999;
        }
        .left-side.sidebar-offcanvas {
            min-height: 100% !important;
        }

        .nowrap {
            white-space: nowrap;
        }
    </style>
</head>
<body class="skin-blue fixed">

    @include('admin::partials.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">

        @include('admin::partials.sidebar')

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side" id="pjax-container">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content-header')
            </section>

            <!-- Main content -->
            <section class="content">
                @include('admin::partials.flashes')
                @yield('content')
            </section>
        </aside>
        <!-- /.right-side -->
    </div>
    <!-- ./wrapper -->

    @yield('script')

    <script>
        jQuery(function($) {
            $(document).ready(function ()
            {
                $.ajaxSetup({
                    beforeSend: function(xhr, settings) {
                        settings.data += "&_token={{ csrf_token() }}";
                    }
                });

                if ($.support.pjax)
                {
                    /*$(document).on('click', 'a:not([no-pjax])', function(event)
                     {
                     var container = $('#pjax-container');
                     $.pjax.click(event, {container: container, fragment: 'pjax-container'});
                     })*/

                    $.pjax.defaults = {
                        timeout: 3000,
                        push: true,
                        replace: false,
                        type: 'GET',
                        dataType: 'html',
                        scrollTo: 0,
                        maxCacheLength: 20,
                        custom_success: function () {

                            var actLink = document.location.protocol + "//" + document.location.hostname + (document.location.port != 80 ? ':' + document.location.port : '') + document.location.pathname;
                            $("a[href='" + actLink + "']").each(function (i, v) {
                                sidebarLinkActive(v);
                            });

                            if (typeof pjaxReady != "undefined")
                                pjaxReady( jQuery );

                            jQuery.ready();
                        }
                    };

                    // Sidebar link active
                    var sidebarLinkActive = function (elem) {
                        if ($(elem).closest('section.sidebar').length) {
                            var parentLi = $(elem).parent('li');
                            parentLi.addClass('active');
                            parentLi.parent('ul.treeview-menu').show();
                            parentLi.closest('li.treeview').addClass('active');
                        }
                    };

                    $(document).on('pjax:complete', function (event, xhr, textStatusm, options) {
                        // run "custom_success" method passed to PJAX if it exists
                        if (typeof options.custom_success === 'function') {
                            options.custom_success();
                        }
                    });

                    $(document).on('click', 'a:not([no-pjax]):not([data-toggle="modal"]):not([href*=#])', function (event)
                    {
                        // Inner link protaction
                        //if ($(this).attr('href')[0] == '#')
                        //    return false;

                        // modal link protection
                        if ( $(this).parents('div.modal').length > 0 )
                            return;

                        $("section.sidebar li.active").removeClass('active');


                        sidebarLinkActive(this);

                        var actLink = $(this).attr('href');
                        $("a[href='" + actLink + "']").each(function (i, v) {
                            sidebarLinkActive(v);
                        });

                        var container = $('#pjax-container');
                        $.pjax.click(event, {container: container, fragment: '#pjax-container'});
                    });

                    /*$(document).pjax('a', '#pjax-container', {
                     fragment: '#pjax-container'
                     });*/
                }
            });
        });
    </script>
</body>
</html>
