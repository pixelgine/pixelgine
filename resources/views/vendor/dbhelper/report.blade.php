@extends($layout)

@section('content-header')
    <h1>
        {!! $title or 'Database Helper Manager' !!}
        {{--
        &middot;
        <small>{!! link_to_route('admin.modulemanager.create', trans('modulemanager::modules.create')) !!}</small>
        --}}
    </h1>
@stop

@section('content')

            <div class="panel panel-default">
                <div class="panel-body">
                    @foreach($tables as $table)
                        @include('dbhelper::table', ['table' => $table])
                    @endforeach
                </div>
            </div>

@stop

@section('script')
    @parent
@stop

@section('style')
    @parent
@stop