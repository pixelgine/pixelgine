@if ( class_exists('\Theme') )
    @extends( Theme::getCurrent() . '::layouts.master')
@else
    @yield('content')
@endif