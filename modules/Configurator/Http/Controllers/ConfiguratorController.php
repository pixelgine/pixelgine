<?php namespace Modules\Configurator\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class ConfiguratorController extends Controller {
	
	public function index()
	{
		return view('configurator::index');
	}
	
}