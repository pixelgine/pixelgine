<?php

Route::group(['prefix' => 'configurator', 'namespace' => 'Modules\Configurator\Http\Controllers'], function()
{
	Route::get('/', 'ConfiguratorController@index');
});