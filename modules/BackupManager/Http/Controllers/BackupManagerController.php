<?php namespace Modules\Backupmanager\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class BackupManagerController extends Controller {
	
	public function index()
	{
		return view('backupmanager::index');
	}
	
}