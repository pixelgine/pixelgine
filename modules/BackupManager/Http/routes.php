<?php

Route::group(['prefix' => 'backupmanager', 'namespace' => 'Modules\BackupManager\Http\Controllers'], function()
{
	Route::get('/', 'BackupManagerController@index');
});