<?php

Route::group(['prefix' => 'forms', 'namespace' => 'Modules\Forms\Http\Controllers'], function()
{
	Route::get('/', 'FormsController@index');
});