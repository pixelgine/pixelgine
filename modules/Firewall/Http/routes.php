<?php

Route::group(['prefix' => 'firewall', 'namespace' => 'Modules\Firewall\Http\Controllers'], function()
{
	Route::get('/', 'FirewallController@index');
});