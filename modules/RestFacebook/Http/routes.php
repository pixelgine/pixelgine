<?php

Route::group(['prefix' => 'restfacebook', 'namespace' => 'Modules\RestFacebook\Http\Controllers'], function()
{
	Route::get('/', 'RestFacebookController@index');
});