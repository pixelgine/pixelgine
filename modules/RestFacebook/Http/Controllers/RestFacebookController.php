<?php namespace Modules\Restfacebook\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RestFacebookController extends Controller {
	
	public function index()
	{
		return view('restfacebook::index');
	}
	
}