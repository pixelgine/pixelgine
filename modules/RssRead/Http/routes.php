<?php

Route::group(['prefix' => 'rssread', 'namespace' => 'Modules\RssRead\Http\Controllers'], function()
{
	Route::get('/', 'RssReadController@index');
});