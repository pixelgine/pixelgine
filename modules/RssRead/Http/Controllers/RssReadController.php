<?php namespace Modules\Rssread\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RssReadController extends Controller {
	
	public function index()
	{
		return view('rssread::index');
	}
	
}