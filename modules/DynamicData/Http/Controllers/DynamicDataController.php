<?php namespace Modules\Dynamicdata\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class DynamicDataController extends Controller {
	
	public function index()
	{
		return view('dynamicdata::index');
	}
	
}