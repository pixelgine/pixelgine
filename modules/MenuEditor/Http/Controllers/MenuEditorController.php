<?php namespace Modules\Menueditor\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class MenuEditorController extends Controller {
	
	public function index()
	{
		return view('menueditor::index');
	}
	
}