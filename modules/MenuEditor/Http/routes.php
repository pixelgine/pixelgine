<?php

Route::group(['prefix' => 'menueditor', 'namespace' => 'Modules\MenuEditor\Http\Controllers'], function()
{
	Route::get('/', 'MenuEditorController@index');
});