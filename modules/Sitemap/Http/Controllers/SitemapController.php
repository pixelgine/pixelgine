<?php namespace Modules\Sitemap\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class SitemapController extends Controller {
	
	public function index()
	{
		return view('sitemap::index');
	}
	
}