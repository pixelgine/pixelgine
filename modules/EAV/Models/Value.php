<?php namespace Modules\Eav\Models;

class Value extends EavBaseModel
{
    protected $table = 'eav_data';

    public function field()
    {
        return $this->hasOne('\Modules\Eav\Models\Attribute', 'id', 'attribute_id');
    }

}