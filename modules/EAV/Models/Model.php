<?php namespace Modules\Eav\Models;

class Model extends EavBaseModel
{
    protected $table = 'eav_model';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['model', 'name', 'description', 'enabled'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Model név alapján
     *
     * @param $query
     * @param $modelName
     * @return mixed
     */
    public function scopeName( $query, $modelName )
    {
        return $query->where('model', '=', $modelName)
            ->first();
    }

    public function scopeEntity( $query, $entityId )
    {
        return $this->hasMany('\Modules\Eav\Models\Entity', 'model_id', 'id')
            ->where('id', '=', $entityId)
            ->first();
    }

    /**
     * Modellhez kapcsolódó auttributumok
     *
     * @return mixed
     */
    public function attributes()
    {
        return $this->hasMany('\Modules\Eav\Models\Attribute', 'model_id', 'id');

    }
    public function __sleep()
    {
        return array();
    }
    /**
     * Modellhez kapcsolódó entitások
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entities()
    {
        //return $this->belongsToMany('\Modules\Eav\Models\Entity', 'eav_data');
        return $this->hasMany('\Modules\Eav\Models\Entity', 'model_id', 'id');
    }

    /**
     * Overrides (Laravel or)Illuminate\Database\Eloquent\Model 's query()
     *
     * @return mixed
     */
    public function newQuery( $excludeDeleted = true )
    {
        $query = parent::newQuery($excludeDeleted);
        $query->orderBy('sort');
        return $query;
    }

}