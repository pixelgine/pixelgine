<?php namespace Modules\Eav\Models;

class Attribute extends EavBaseModel
{
    protected $table = 'eav_attribute';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['model_id', 'attribute', 'slug', 'type', 'validation'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected static $enumTypes  =
    [
        'textandnumber' =>
        [
            'CHAR'      => [
                'input' => 'text',
                'valid' => ['numeric','min:1','max:1'],
                'group' => 1,
            ],
            'FLOAT'     => [
                'input' => 'number',
                'valid' => ['numeric'],
                'group' => 1,
            ],
            'INTEGER'   => [
                'input' => 'number',
                'valid' => ['integer|digits_between:-32768,65535'],
                'group' => 1,
            ],
            'STRING'    => [
                'input' => 'text',
                'valid' => ['string|max:255'],
                'group' => 1,
            ],
            'TEXT'      => [
                'input' => 'textarea',
                'valid' => ['alpha_num|max:65500'],
                'group' => 1,
            ],
            'WYSIWYG'   => [
                'input' => 'textarea',
                'valid' => ['alpha_num|max:65500'],
                'group' => 1,
            ],
        ],
        'form' =>
        [
            'BOOLEAN'   => [
                'input' => 'textarea',
                'valid' => ['boolean'],
                'group' => 2,
            ],
            'EMAIL'     => [
                'input' => 'text',
                'valid' => ['email|max:255'],
                'group' => 2,
            ],
        ],
        'dateandtime' =>
        [
            'DATE'      => [
                'input' => 'date',
                'valid' => ['date_format:"Y-m-d"|regex:/[0-9]{4}-[0-9]{2}-[0-9]{2}/|max:10'],
                'group' => 3,
            ],
            'DATETIME'  => [
                'input' => 'datetime',
                'valid' => ['date_format:"Y-m-d H:i:s"|regex:/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/|max:19'],
                'group' => 3,
            ],
            'TIME'      => [
                'input' => 'time',
                'valid' => ['date_format:"H:i:s"|regex:/[0-9]{2}:[0-9]{2}:[0-9]{2}/|max:8'],
                'group' => 3,
            ],
            'TIMEZONE'  => [
                'input' => 'text',
                'valid' => ['timezone'],
                'group' => 3,
            ],
        ],
        'storage' =>
        [
            'FILE'      => [
                'input' => 'file',
                'valid' => [''],
                'group' => 4,
            ],
            'IMAGE'     => [
                'input' => 'file',
                'valid' => ['image'],
                'group' => 4,
            ],
        ],
    ];

    protected static $enums = [];
    protected static $enumNames = [];
    protected static $enumInputs = [];

    public function scopeEnumList()
    {
        if ( !empty( self::$enums ) )
            return self::$enums;

        // Setting attribute type enumeration list
        foreach( self::$enumTypes AS $groupKey => $group )
        {
            $groupName = trans('eav::enum.group.' . $groupKey);
            self::$enums[ $groupName ] = [];
            foreach( $group AS $enumKey => $enum )
            {
                self::$enums[ $groupName ][ $enumKey ] = trans('eav::enum.' . $enumKey);
            }
        }

        return self::$enums;

    }

    public function scopeEnumNames()
    {
        $this->scopeEnumList();

        if ( empty( self::$enumNames ) )
        foreach( self::$enums AS $keyOff => $enumList )
            foreach( $enumList AS $enumName => $enumText )
                self::$enumNames[] = $enumName;

        return self::$enumNames;
    }

    public function scopeEnumInput()
    {
        $this->scopeEnumList();

        // Setting attribute type enumeration list
        foreach( self::$enumTypes AS $groupKey => $group )
        {
            foreach( $group AS $enumKey => $enum )
            {
                self::$enumInputs[ $enumKey ] = $enum['input'];
            }
        }

        return self::$enumInputs;
    }

    /**
     * Overrides (Laravel or)Illuminate\Database\Eloquent\Model 's query()
     *
     * @return mixed
     */
    public function newQuery( $excludeDeleted = true )
    {
        $query = parent::newQuery($excludeDeleted);
        $query->orderBy('sort');
        return $query;
    }

}