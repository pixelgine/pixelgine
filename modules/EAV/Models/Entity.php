<?php namespace Modules\Eav\Models;

class Entity extends EavBaseModel
{
    protected $table  = 'eav_entity';

    /**
     * Entitáshoz tartozó attributumok
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    /*public function fields()
    {
        //return $this->hasMany('\Modules\Eav\Models\Attribute', 'entity_id', 'id');
        return $this->belongsTo('\Modules\Eav\Models\Attribute');
        //return $this->hasMany('\Modules\Eav\Models\Attribute');
    }*/

    /**
     * Eredmények listázása
     * attr_id => value
     *
     * @return array
     */
    public function scopeList(  )
    {
        $resultAttributes = [];

        // Get attributes
        $attributes =
            Attribute::where('model_id', '=', $this->model_id)
            ->whereNull('deleted_at')
            ->get();

        // Get values
        $value = $this
            ->hasMany('\Modules\Eav\Models\Value')
            ->whereNull('deleted_at')
            ->get();

        // Attribute result list init
        foreach( $attributes AS $attr )
            $resultAttributes[ $attr->id ] = [];

        // Merge values with attributes
        foreach( $value AS $val )
            if ( isset($resultAttributes[ $val->attribute_id ]) )
                $resultAttributes[ $val->attribute_id ] = $val;

        return $resultAttributes;

    }

    public function save(array $options = array())
    {
        if ( !$this->id )
            return parent::save( $options );

        $attributes = $this->parseModelParams();

        $this->updateOrSaveModel( $attributes );

    }

    public function update(array $attributes = [])
    {
        foreach( $attributes As $k => $v )
            $this->{$k} = $v;

        $this->save();
    }

    public static function create(array $attributes = [])
    {
        Attribute::unguard();
        $entity = parent::create( $attributes );
        $entity->save();
        Attribute::reguard();

        return $entity;
    }

    /**
     * ??
     * Összes value listázása??
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany('\Modules\Eav\Models\Value');
    }

    public function model()
    {
        return $this->belongsTo('\Modules\Eav\Models\Model');
    }

    public function attributes()
    {
        return $this->hasMany('\Modules\Eav\Models\Attribute', 'model_id');
    }

    private function updateOrSaveModel( $attributes )
    {
        foreach( $attributes AS $attr => $val )
        {
            if ( $val )
            {
                $value = Value::where('attribute_id', '=', $attr)
                    ->where('entity_id', '=', $this->id)
                    ->first();

                if ( !$value )
                {
                    $value = new Value();
                    $value->attribute_id = $attr;
                    $value->entity_id    = $this->id;
                }

                $value->value = $val;
                $value->save();
            }
        }
    }

    private function parseModelParams()
    {
        $list = [];
        $attrs =
            Attribute::where('model_id', '=', $this->model_id)
                ->whereNull('deleted_at')
                ->get()->lists('slug', 'id');

        foreach( $attrs AS $key => $attr )
        {
            if ( isset($this->{$attr}) )
            {
                $list[$key] = $this->{$attr};
                unset($this->{$attr});
            }
            else
            {
                $list[$key] = null;
            }
        }

        return $list;
    }
}