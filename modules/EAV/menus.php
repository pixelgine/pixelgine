<?php

    $leftMenu  = Menu::instance('admin-menu');
    $rightMenu = Menu::instance('admin-menu-right');

    /**
     * @see https://github.com/pingpong-labs/menus
     *
     * @example adding additional menu.
     *
     * $leftMenu->url('your-url', 'The Title');
     *
     * $leftMenu->route('your-route', 'The Title');
     */
    $leftMenu->enableOrdering();
    $leftMenu->route('admin.eav.index', trans('eav::eav.title'), [], 100, ['icon' => 'fa fa-database']);

    $dataList = \Modules\Eav\Models\Model::where('enabled', '=', '1')->get();
    $leftMenu->whereTitle( trans('eav::eav.title'), function ($sub) use ($dataList)
    {
        foreach( $dataList AS $model )
            $sub->url( route('admin.eav.entity.index', $model->id), $model->name );
        $sub->header('<hr style="margin: 0;">');
        $sub->url( route('admin.eav.index'), trans('eav::eav.all_title') );

    }, 100, ['icon' => 'fa fa-puzzle-piece']);

/*
    $leftMenu->dropdown(trans('modulemanager::modules.title'), function ($sub) {
        $sub->route('admin.modulemanager.index',  trans('modulemanager::modules.all'),    [], 1);
        $sub->route('admin.modulemanager.create', trans('modulemanager::modules.create'), [], 2);
    }, 100, ['icon' => 'fa fa-puzzle-piece']);
*/
