EAV kiegészítése:-
- lehessen csinálni az adatokból hard táblát is!!!
	- migráció készítése
	- migráció futtatása
	- adatok átmásolása
	- model készítése
	- model app könyvtárba másolása

- És így is lehet adatokat kezelni a továbbiakban,
http://badgeek.github.io/laraveldb/#

Itt annyi lenne, hogy van egy EAV tábla, akkor abból lehessen hard táblát csinálni.
Valószínűleg erre akkor lesz szükség, ha sok adattal kell foglalkozni.


Ezt majd tesztelni kell, hogy 1000, 5000, 10.000, 50.000, 100.000 sornál mennyi a külömbség.

Tábla felépítése:
id (entity_id) - attr_1_slug - attr_2_slug - ... - created_at - updated_at - deleted_at
