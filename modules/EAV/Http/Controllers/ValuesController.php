<?php namespace Modules\Eav\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Modules\Eav\Models\Entity;
use Modules\Eav\Models\Model;
use Modules\Eav\Models\Value;
use Pingpong\Modules\Routing\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use Modules\Eav\Forms\EntityCreateForm;

class ValuesController extends Controller
{
	protected $enums = [];
	protected $enumNames = [];
	protected $form  = null;

	public function store()
	{
		$input = Input::all();
		$value = Value::find($input['pk']);

		$rules = array(
			'name'             => $value->field->validation
		);

		$validator = \Validator::make(Input::only('name'), $rules, [], [
			'name' => $value->field->attribute
		]);

		// check if the validator failed
		if ($validator->fails()) {

			// get the error messages from the validator
			$messages = (array)$validator->messages();
			$message  = reset($messages);

			return \Response::make( reset($message['name']), 400);
		}

		$value->value = $input['value'];
		$value->save();
	}
}