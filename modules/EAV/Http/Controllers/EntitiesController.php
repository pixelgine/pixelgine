<?php namespace Modules\Eav\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Kris\LaravelFormBuilder\FormBuilder;
use Modules\Eav\Models\Attribute;
use Modules\Eav\Models\Entity;
use Pingpong\Modules\Routing\Controller;
use Modules\Eav\Forms\EntityCreateForm;
use Modules\Eav\Models\Model;

use Illuminate\Support\Facades\Input;

class EntitiesController extends Controller
{
	/**
	 * Listing entities of model
	 *
	 * @param FormBuilder $formBuilder
	 * @param $id
	 * @return \Illuminate\View\View
	 */
	public function index( FormBuilder $formBuilder, $id )
	{
		$model = Model::find( $id );

		$form = $formBuilder->create(EntityCreateForm::class, [
			'method' => 'POST',
			'url'  	 => route('admin.eav.entity.store')
		])->modify('model_id', 'hidden', [
			'value' => $model->id
		]);

		$enumInput = Attribute::enumInput();

		foreach( $model->attributes AS $attr )
		{
			$inputType = isset($enumInput[$attr->type]) ? $enumInput[$attr->type] : 'text';
			$form = $form->add($attr->slug, $inputType, [
				'rules' => $attr->validation,
				'label' => false
			]);
		}

		$enumInput = Attribute::enumInput();

		return view('eav::admin.entity.index', compact('model','form','enumInput'));
	}

	/**
	 * Store new entity
	 *
	 * @param FormBuilder $formBuilder
	 * @return $this
	 */
	public function store( FormBuilder $formBuilder )
	{
		//dd(Input::all());
		$form = $formBuilder->create(EntityCreateForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if ( !$form->isValid() )
		{
			return redirect()->back()->withErrors(
				$form->getErrors()
			)->withInput();
		}

		$input  = Input::all();
		$entity = Entity::create([
			'model_id' => $input['model_id']
		]);
		$entity->update($input);

		if ( !$entity )
		{
			return Redirect::back()
				->withFlashMessage( trans('eav::messages.entity.store.error') )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( trans('eav::messages.entity.store.success') )
			->withFlashType('success');
	}
}