<?php namespace Modules\Eav\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Modules\Eav\Models\Attribute;
use Modules\Eav\Models\Model;
use Pingpong\Modules\Routing\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use Modules\Eav\Forms\ModelCreateForm;
use Modules\Eav\Forms\AttributeCreateForm;

class ModelsController extends Controller
{
	/**
	 * Models listing with store
	 *
	 * @param FormBuilder $formBuilder
	 * @return \Illuminate\View\View
	 */
	public function index( FormBuilder $formBuilder )
	{
		$models = Model::all();
		$form = $formBuilder->create(ModelCreateForm::class, [
			'method' => 'POST',
			'url'  	 => route('admin.eav.model.store')
		]);

		return view('eav::admin/index', compact('models','form'));
	}

	/**
	 * Model details and edit page
	 *
	 * @param FormBuilder $formBuilder
	 * @param $id
	 * @return \Illuminate\View\View
	 */
	public function edit( FormBuilder $formBuilder, $id )
	{
		//$model = Model::name('Contacts');
		//dd($model->entity(1)->list());
		//dd($model->entity(1)->value);
		//dd($model->attributes[0]->values);


		//$user = $model->entity(1);
		//dd($user);
		//$user->hehe = 'aasda123';
		//$user->save();


		$model = Model::find( $id );
		$enums = Attribute::enumList();
		$enumNames = Attribute::enumNames();

		$form = $formBuilder->create(AttributeCreateForm::class, [
			'method' => 'POST',
			'url'  	 => route('admin.eav.attribute.store')
		])->modify('type', 'select', [
			'choices' => $enums,
			'rules'   => 'required|in:' . implode(',', $enumNames),
		])->modify('model_id', 'hidden', [
			'value' => $id
		]);

		return view('eav::admin/edit', compact('model','form','enums'));
	}

	/**
	 * Store model details
	 *
	 * @param FormBuilder $formBuilder
	 * @return mixed
	 */
	public function store( FormBuilder $formBuilder )
	{
		$form = $formBuilder->create(ModelCreateForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if ( !$form->isValid() )
		{
			return redirect()->back()->withErrors(
				$form->getErrors()
			)->withInput();
		}

		$input = Input::all();
		$model = Model::create();
		$model->update($input);

		if ( !$model )
		{
			return Redirect::back()
				->withFlashMessage( trans('eav::model.action.store.danger', ['name' => $input['name']]) )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( trans('eav::model.action.store.success', ['name' => $input['name']]) )
			->withFlashType('success');
	}

	/**
	 * Enable model usage
	 *
	 * @param $id
	 * @return mixed
	 */
	public function enable( $id )
	{
		$model = Model::find( $id );
		$model->enabled = 1;

		if ( !$model->save() )
		{
			return Redirect::route('admin.eav.index')
				->withFlashMessage( trans('eav::model.action.enable.danger', ['name' => $model->model]) )
				->withFlashType('danger');
		}

		return Redirect::route('admin.eav.index')
			->withFlashMessage( trans('eav::model.action.enable.success', ['name' => $model->model]) )
			->withFlashType('success');
	}

	/**
	 * Disable model usage
	 *
	 * @param $id
	 * @return mixed
	 */
	public function disable( $id )
	{
		$model = Model::find( $id );
		$model->enabled = 0;

		if ( !$model->save() )
		{
			return Redirect::route('admin.eav.index')
				->withFlashMessage( trans('eav::model.action.disable.danger', ['name' => $model->model]) )
				->withFlashType('danger');
		}

		return Redirect::route('admin.eav.index')
			->withFlashMessage( trans('eav::model.action.disable.success', ['name' => $model->model]) )
			->withFlashType('success');
	}
}