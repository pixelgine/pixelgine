<?php


Route::group(['prefix' => config('admin.prefix', 'admin'), 'namespace' => 'Modules\EAV\Http\Controllers'], function()
{
	Route::group(['middleware' => config('admin.filter.auth')], function ()
	{
		///--- EAV ---///
		/*Route::resource('eav', 'EAVController', [
			'except' => ['create','store','show','update','destroy'],
			'names' => [
				'index' 	=> 'admin.eav.index',
				'create' 	=> 'admin.eav.create',
				'store' 	=> 'admin.eav.store',
				'show' 		=> 'admin.eav.show',
				'update' 	=> 'admin.eav.update',
				'edit' 		=> 'admin.eav.edit',
				'destroy' 	=> 'admin.eav.destroy',
			],
		]);

		Route::get('eav/{id}/enable',  ['as' => 'admin.eav.enable',  'uses' => 'EAVController@enable'] );
		Route::get('eav/{id}/disable', ['as' => 'admin.eav.disable', 'uses' => 'EAVController@disable']);
		Route::get('eav/{id}/data',    ['as' => 'admin.eav.data',  	 'uses' => 'EAVController@data'] );*/

		///--- Models ---///
		Route::resource('eav', 'ModelsController', [
			'except'=> ['index'],
			'names' => [
				//'index' 	=> 'admin.eav.model.index',
				'create' 	=> 'admin.eav.model.create',
				'store' 	=> 'admin.eav.model.store',
				'show' 		=> 'admin.eav.model.show',
				'update' 	=> 'admin.eav.model.update',
				'edit' 		=> 'admin.eav.model.edit',
				'destroy' 	=> 'admin.eav.model.destroy',
			],
		]);
		Route::get('eav',  			   ['as' => 'admin.eav.index',  	   'uses' => 'ModelsController@index']  );
		Route::get('eav/{id}/enable',  ['as' => 'admin.eav.model.enable',  'uses' => 'ModelsController@enable'] );
		Route::get('eav/{id}/disable', ['as' => 'admin.eav.model.disable', 'uses' => 'ModelsController@disable']);

		///--- Entities ---///
		Route::resource('eav/{id}/entity', 'EntitiesController', [
			'names' => [
				'index' 	=> 'admin.eav.entity.index',
				'create' 	=> 'admin.eav.entity.create',
				'store' 	=> 'admin.eav.entity.store',
				'show' 		=> 'admin.eav.entity.show',
				'update' 	=> 'admin.eav.entity.update',
				'edit' 		=> 'admin.eav.entity.edit',
				'destroy' 	=> 'admin.eav.entity.destroy',
			],
		]);

		///--- Values ---///
		Route::get('eav/{id}/values', [
			'as' => 'admin.eav.value.show',   'uses' => 'ValuesController@show'
		]);
		Route::post('eav/{id}/value/{id_val}', [
			'as' => 'admin.eav.value.store',  'uses' => 'ValuesController@store'
		]);

		///--- Attributes ---///
		Route::post('eav/attribute', [
			'as' => 'admin.eav.attribute.store',  'uses' => 'AttributesController@store'
		]);
		Route::get('eav/attribute/{id}/destroy',  [
			'as' => 'admin.eav.attribute.destroy',  'uses' => 'AttributesController@destroy'
		]);




	});
});
