<?php namespace Modules\Eav\Forms;

use Kris\LaravelFormBuilder\Form as Form;

class ValueEditForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('entity_id', 'hidden', [
                'rules' => 'required|integer',
                'label' => false
            ])
            ->add('save', 'submit', [
                'label' => 'Add'
            ]);
    }
}