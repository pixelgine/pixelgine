<?php namespace Modules\Eav\Forms;

use Kris\LaravelFormBuilder\Form as Form;

class ModelCreateForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('model', 'text', [
                'rules' => 'required|min:3|max:64',
                'label' => false
            ])
            ->add('name', 'text', [
                'rules' => 'required|min:3|max:64',
                'label' => false
            ])
            ->add('description', 'text', [
                'rules' => 'required|min:3|max:250',
                'label' => false
            ])
            ->add('save', 'submit', [
                'label' => 'Add'
            ]);
    }
}