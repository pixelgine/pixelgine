<?php namespace Modules\Eav\Forms;

use Kris\LaravelFormBuilder\Form as Form;

class AttributeCreateForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('attribute', 'text', [
                'rules' => 'required|min:3|max:64',
                'label' => false
            ])
            ->add('slug', 'text', [
                'rules' => 'required|min:3|max:32',
                'label' => false
            ])
            ->add('type', 'select', [
                'empty_value' => trans('eav::action.select'),
                'label' => false,
            ])
            ->add('validation', 'text', [
                'rules' => '',
                'label' => false
            ])
            ->add('model_id', 'hidden', [
                'rules' => 'required|integer',
                'label' => false
            ])
            ->add('save', 'submit', [
                'label' => 'Add'
            ]);
    }
}