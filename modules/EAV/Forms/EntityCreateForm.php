<?php namespace Modules\Eav\Forms;

use Kris\LaravelFormBuilder\Form as Form;

class EntityCreateForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('model_id', 'hidden', [
                'rules' => 'required|integer',
                'label' => false
            ])
            ->add('save', 'submit', [
                'label' => 'Add'
            ]);
    }
}