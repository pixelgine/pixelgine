<?php
return [
    'group'     => [
        'textandnumber' => 'Szöveg és szám1',
        'form'          => 'Form űrlap',
        'dateandtime'   => 'Dátum és idő',
        'storage'       => 'Tárolás',
    ],

    'CHAR'      => 'Karakter',
    'FLOAT'     => 'Tört szám',
    'INTEGER'   => 'Egész szám',
    'STRING'    => 'Egysoros szöveg',
    'TEXT'      => 'Többsoros szöveg',
    'WYSIWYG'   => 'Formázott szöveg',

    'BOOLEAN'   => 'Jelölő négyzet',
    'EMAIL'     => 'E-mail',

    'DATE'      => 'Dátum',
    'DATETIME'  => 'Dátum és idő',
    'TIME'      => 'Idő',
    'TIMEZONE'  => 'Időzóna',

    'FILE'      => 'Fájl',
    'IMAGE'     => 'Kép',
];