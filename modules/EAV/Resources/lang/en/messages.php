<?php
return [
    'entity' => [
        'store' => [
            'success' => 'Entity created!',
            'error'   => 'Entity not created!',
        ]
    ],
];