<?php
return [
    'name'  => 'Attribute',
    'table' => [
        'slug'          => 'Slug',
        'name'          => 'Name',
        'type'          => 'Field type',
        'validation'    => 'Validation',
        'description'   => 'Description',
    ],
];