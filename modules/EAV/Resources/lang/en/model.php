<?php
return [
    'name'  => 'Model',
    'table' => [
        'model'         => 'Model',
        'name'          => 'Name',
        'description'   => 'Description',
    ],
    'action' => [
        'delete'    => 'Delete&nbsp;model'
    ]
];