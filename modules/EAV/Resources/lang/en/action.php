<?php
return [
    'name'      => 'Action',
    'back'      => 'Back',
    'edit'      => 'Edit',
    'enable'    => 'Enable',
    'disable'   => 'Disable',
    'delete'    => 'Delete',
    'create'    => 'Create',
    'select'    => 'Select',

    'data_list' => 'Data&nbsp;list',
    'attr_list' => 'Attribute&nbsp;list',
    'trash_list'=> 'Trash&nbsp;list',

    'created_at'=> 'Created',
    'updated_at'=> 'Updated',
    'deleted_at'=> 'Deleted',
];