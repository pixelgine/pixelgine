<?php
return [
    'group'     => [
        'textandnumber' => 'Text and number',
        'form'          => 'Form',
        'dateandtime'   => 'Date and time',
        'storage'       => 'Storage',
    ],

    'CHAR'      => 'Character',
    'FLOAT'     => 'Float number',
    'INTEGER'   => 'Integer',
    'STRING'    => 'Single line text',
    'TEXT'      => 'Multi line text',
    'WYSIWYG'   => 'Formatted text',

    'BOOLEAN'   => 'Boolean',
    'EMAIL'     => 'E-mail',

    'DATE'      => 'Date',
    'DATETIME'  => 'Date and time',
    'TIME'      => 'Time',
    'TIMEZONE'  => 'Timezone',

    'FILE'      => 'File',
    'IMAGE'     => 'Image',
];