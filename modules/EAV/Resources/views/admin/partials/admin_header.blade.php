<h1>
    {!! $title or 'All EAV Models' !!}
    @if( isset($models) )
    ({!! count($models) !!})
    @endif
    &middot;
    <small>{!! link_to_route('admin.eav.create', trans('eav::models.create')) !!}</small>
</h1>