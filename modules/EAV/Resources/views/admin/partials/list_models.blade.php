{!! form_start($form) !!}
<table class="table">
    <thead>
    <tr>
        <th width="1"></th>
        <th>{{ trans('eav::model.table.name') }}</th>
        <th>{{ trans('eav::model.table.model') }}</th>
        <th>{{ trans('eav::model.table.description') }}</th>
        <th width="1" class="text-center">{{ trans('eav::action.name') }}</th>
    </tr>
    </thead>
    <tbody>
    @forelse($models as $model)
    <tr>
        <td><i class="fa fa-{!! $model->enabled ? 'check' : 'times' !!}"></i></td>
        <td>{!! $model->name !!}</td>
        <td>{!! $model->model !!}</td>
        <td>{!! $model->description !!}</td>
        <td class="text-center">
            <a href="{!! route('admin.eav.entity.index', $model->id) !!}">{{ trans('eav::action.data_list') }}</a>
            &middot;
            <a href="{!! route('admin.eav.model.edit', $model->id) !!}">{{ trans('eav::action.attr_list') }}</a>
            &middot;
        @if ($model->enabled)
            {!! modal_popup(route('admin.eav.model.disable', $model->id), trans('eav::action.disable'), trans('modulemanager::model.modal.disable', ['name' => $model->model])) !!}
        @else
            {!! modal_popup(route('admin.eav.model.enable',  $model->id), trans('eav::action.enable'),  trans('modulemanager::model.modal.enable',  ['name' => $model->model])) !!}
        @endif
        </td>
    </tr>
    @empty
    <tr>
        <td colspan="5" class="text-center">
            {{ trans('eav::model.table.empty_data') }}
        </td>
    </tr>
    @endforelse
    </tbody>
    <tfoot>
    <tr>
        <td></td>
        <td>{!! form_row($form->name) !!}</td>
        <td>{!! form_row($form->model) !!}</td>
        <td>{!! form_row($form->description) !!}</td>
        <td>
            {!! form_row($form->save) !!}
        </td>
    </tr>
    </tfoot>
</table>
{!! form_end($form, false) !!}