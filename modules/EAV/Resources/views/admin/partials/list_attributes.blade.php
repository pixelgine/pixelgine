{!! form_start($form) !!}
<table class="table">
    <thead>
    <tr>
        <th>{{ trans('eav::attribute.table.name') }}</th>
        <th>{{ trans('eav::attribute.table.slug') }}</th>
        <th>{{ trans('eav::attribute.table.type') }}</th>
        <th>{{ trans('eav::attribute.table.validation') }}</th>
        <th width="1" class="text-center">{{ trans('eav::action.name') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $model->attributes AS $attribute )
        {{-- dd($attribute) --}}
        <tr>
            <td>{{ $attribute->attribute }}</td>
            <td>{{ $attribute->slug }}</td>
            <td>{{ array_recursive_find($enums, $attribute->type) }}</td>
            <td>{{ $attribute->validation }}</td>
            <td>{{-- modal_popup( route('admin.eav.attribute.destroy', $attribute->id), 'Delete', trans('eav::attribute.modal.delete', ['name' => $attribute->attribute])) --}}</td>
        </tr>
    @endforeach
    </tbody>
    <tbody>
    <tr>
        <td>{!! form_row($form->attribute) !!}</td>
        <td>{!! form_row($form->slug) !!}</td>
        <td>{!! form_row($form->type) !!}</td>
        <td>{!! form_row($form->validation) !!}</td>
        <td>
            {!! form_row($form->model_id) !!}
            {!! form_row($form->save) !!}
        </td>
    </tr>
    </tbody>
</table>
{!! form_end($form, false) !!}
