@extends($layout)

@section('content-header')
	<h1>
		{!! $title or 'All EAV Models' !!}
		@if( isset($models) )
			({!! count($models) !!})
		@else
			(0)
		@endif
		{{--
        &middot;
        <small>{!! link_to_route('admin.eav.model.create', trans('eav::models.create')) !!}</small>
        --}}
	</h1>
@stop

@section('content')


	@if(isset($search))
		@include('admin::users.search-form')
	@endif

	@include('eav::admin/partials/list_models', ['form'=>$form, 'models' => $models])

@stop

@section('script')
	@parent

	<script>
		$(document).ready(function()
		{
			//http://stackoverflow.com/questions/1026069/capitalize-the-first-letter-of-string-in-javascript
		});
	</script>
@stop
