@extends($layout)

@section('content-header')
	<h1>
		{{ $model->model }} EAV Model Entities
		&middot;
		<small>{!! link_to_route('admin.eav.index', trans('eav::action.back')) !!}</small>
		&middot;
		<small>{!! link_to_route('admin.eav.model.edit', trans('eav::action.attr_list'), [$model->id]) !!}</small>
		&middot;
		<small>{!! link_to_route('admin.eav.entity.index', trans('eav::action.data_list'), [$model->id]) !!}</small>
	</h1>
@stop

@section('content')

	@include('eav::admin.entity.table', ['form'=>$form, 'model' => $model, 'enumInput' => $enumInput])

@stop

@section('script')
	@parent

	<script>
		function pjaxReady()
		{
			// Search off
			$("input[name='q'], #search-btn").attr('disabled', true);

			$.ajaxSetup({
				beforeSend: function(xhr, settings) {
					settings.data += "&_token={{ csrf_token() }}";
				}
			});

			$('.editable').editable().on('hidden', function(e, reason)
			{
				/*if ( reason === 'save' )
				 {
				 var value = $(this).val();
				 var store = $(this).data('url');

				 }

				 alert(reason);
				 */
			});
		}

		jQuery(document).ready(pjaxReady);
	</script>

@stop

@section('style')
	@parent

	<style>
		a.status-1{
			font-weight: bold;
		}
	</style>
@stop