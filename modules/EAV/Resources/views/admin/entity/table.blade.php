{!! form_start($form) !!}
<table class="table">
    <thead>
    <tr>
        <th>#</th>
        @foreach( $modelAttributes = $model->attributes AS $attribute )
            <th id="{!! $attributesIndex[$attribute->id]['slug'] = $attribute->slug !!}-{!! $attributesIndex[$attribute->id]['type'] = $attribute->type !!}">{{ $attribute->attribute }}</th>
        @endforeach
        <th width="1" class="text-center">{{ trans('eav::action.name') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $model->entities AS $entity )
    <tr>
        <td width="1">{{ $entity->id }}</td>
        {{-- $model->entity( $entity->id )->list() --}}
        @foreach( $entity->list() AS $data )
        <td>
            @if ( $data )
            <a href="#edit"
               @if( $enumInput[ $attributesIndex[$data->attribute_id]['type'] ] != 'file' )
               class="editable"
               @else
               class="disabled" disabled=""
               @endif
               id="data-{{ $data->id }}-{{ $attributesIndex[$data->attribute_id]['slug'] }}"
               data-pk="{{ $data->id }}"
               data-type="{{ $enumInput[ $attributesIndex[$data->attribute_id]['type'] ] }}"
               data-name="{{ $attributesIndex[$data->attribute_id]['slug'] }}"
               data-url="{{ route('admin.eav.value.store', ['id'=>$model->id, 'id_val'=>$data->attribute_id]) }}"
               data-placement="bottom">
                {{ $data->value }}
            </a>
            @endif
        </td>
        @endforeach
        <td>
            {{-- modal_popup(route('admin.eav.enable',  $model->id), trans('eav::model.delete'),  trans('modulemanager::modules.modal.delete',  ['name' => $model->model])) --}}
        </td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td></td>
        @foreach( $modelAttributes AS $attribute )
            <td>{!! form_row($form->{$attribute->slug}) !!}</td>
        @endforeach
        <td>
            {!! form_row($form->model_id) !!}
            {!! form_row($form->save) !!}
        </td>
    </tr>
    </tfoot>
</table>
{!! form_end($form, false) !!}