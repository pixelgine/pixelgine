@extends($layout)

@section('content-header')
	<h1>
		EAV Model Create
		&middot;
		<small>{!! link_to_route('admin.eav.index', trans('eav::model.back')) !!}</small>
	</h1>)
@stop

@section('content')
	<div>
		@include('admin::users.form')
	</div>

@stop
