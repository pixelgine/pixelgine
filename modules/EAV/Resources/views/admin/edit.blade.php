@extends($layout)

@section('content-header')
	<h1>
		{{ $model->model }} EAV Model
		&middot;
		<small>{!! link_to_route('admin.eav.index', trans('eav::action.back')) !!}</small>
		&middot;
		<small>{!! link_to_route('admin.eav.model.edit', trans('eav::action.attr_list'), [$model->id]) !!}</small>
		&middot;
		<small>{!! link_to_route('admin.eav.entity.index', trans('eav::action.data_list'), [$model->id]) !!}</small>
	</h1>
@stop

@section('content')
	{{--
	<div>
		@include('modulemanager::admin.form', array('model' => $module))
	</div>
	--}}

	<div>
		<table class="table">
			<thead>
				<tr>
					<th>{{ trans('eav::model.table.name') }}</th>
					<th>{{ trans('eav::model.table.description') }}</th>
					<th>{{ trans('eav::model.table.alias') }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $model->model }}</td>
					<td>{{ $model->description }}</td>
					<td></td>
				</tr>
			</tbody>
		</table>

		@include('eav::admin/partials/list_attributes', ['form'=>$form, 'model' => $model])

		{!! modal_popup(route('admin.eav.model.destroy', $model->id), trans('eav::model.action.delete'),  trans('modulemanager::model.modal.delete',  ['name' => $model->model])) !!}
	</div>
@stop
