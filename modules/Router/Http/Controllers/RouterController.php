<?php namespace Modules\Router\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RouterController extends Controller {
	
	public function index()
	{
		return view('router::index');
	}
	
}