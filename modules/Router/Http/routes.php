<?php

Route::group(['prefix' => 'router', 'namespace' => 'Modules\Router\Http\Controllers'], function()
{
	Route::get('/', 'RouterController@index');
});