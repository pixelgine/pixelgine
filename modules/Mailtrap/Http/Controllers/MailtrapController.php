<?php namespace Modules\Mailtrap\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class MailtrapController extends Controller {
	
	public function index()
	{
		return view('mailtrap::index');
	}
	
}