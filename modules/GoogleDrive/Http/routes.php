<?php

Route::group(['prefix' => 'googledrive', 'namespace' => 'Modules\GoogleDrive\Http\Controllers'], function()
{
	Route::get('/', 'GoogleDriveController@index');
});