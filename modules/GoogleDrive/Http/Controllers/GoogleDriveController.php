<?php namespace Modules\Googledrive\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class GoogleDriveController extends Controller {
	
	public function index()
	{
		return view('googledrive::index');
	}
	
}