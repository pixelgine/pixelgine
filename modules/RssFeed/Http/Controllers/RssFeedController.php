<?php namespace Modules\Rssfeed\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RssFeedController extends Controller {
	
	public function index()
	{
		return view('rssfeed::index');
	}
	
}