<?php

Route::group(['prefix' => 'rssfeed', 'namespace' => 'Modules\RssFeed\Http\Controllers'], function()
{
	Route::get('/', 'RssFeedController@index');
});