<?php namespace Modules\Officeppt\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class OfficePptController extends Controller {
	
	public function index()
	{
		return view('officeppt::index');
	}
	
}