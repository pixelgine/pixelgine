<?php

Route::group(['prefix' => 'officeppt', 'namespace' => 'Modules\OfficePpt\Http\Controllers'], function()
{
	Route::get('/', 'OfficePptController@index');
});