<?php

Route::group(['prefix' => 'messenger', 'namespace' => 'Modules\Messenger\Http\Controllers'], function()
{
	Route::get('/', 'MessengerController@index');
});