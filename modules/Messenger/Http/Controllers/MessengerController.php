<?php namespace Modules\Messenger\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class MessengerController extends Controller {
	
	public function index()
	{
		return view('messenger::index');
	}
	
}