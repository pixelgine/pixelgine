<?php namespace Modules\Restredmine\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RestRedmineController extends Controller {
	
	public function index()
	{
		return view('restredmine::index');
	}
	
}