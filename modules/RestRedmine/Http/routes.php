<?php

Route::group(['prefix' => 'restredmine', 'namespace' => 'Modules\RestRedmine\Http\Controllers'], function()
{
	Route::get('/', 'RestRedmineController@index');
});