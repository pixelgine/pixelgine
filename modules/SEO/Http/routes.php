<?php

Route::group(['prefix' => 'seo', 'namespace' => 'Modules\SEO\Http\Controllers'], function()
{
	Route::get('/', 'SEOController@index');
});