<?php namespace Modules\Seo\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class SEOController extends Controller {
	
	public function index()
	{
		return view('seo::index');
	}
	
}