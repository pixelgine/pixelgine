<?php

Route::group(['prefix' => 'pushnotification', 'namespace' => 'Modules\Pushnotification\Http\Controllers'], function()
{
	Route::get('/', 'PushnotificationController@index');
});