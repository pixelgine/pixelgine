<?php

Route::group(['prefix' => 'officepdf', 'namespace' => 'Modules\OfficePdf\Http\Controllers'], function()
{
	Route::get('/', 'OfficePdfController@index');
});