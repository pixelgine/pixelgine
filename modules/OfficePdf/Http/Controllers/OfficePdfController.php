<?php namespace Modules\Officepdf\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class OfficePdfController extends Controller {
	
	public function index()
	{
		return view('officepdf::index');
	}
	
}