<?php

Route::group(['prefix' => 'resttwitter', 'namespace' => 'Modules\RestTwitter\Http\Controllers'], function()
{
	Route::get('/', 'RestTwitterController@index');
});