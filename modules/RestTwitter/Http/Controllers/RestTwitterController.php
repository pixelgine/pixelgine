<?php namespace Modules\Resttwitter\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RestTwitterController extends Controller {
	
	public function index()
	{
		return view('resttwitter::index');
	}
	
}