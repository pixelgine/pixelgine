<?php namespace Modules\Googleanalytics\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class GoogleAnalyticsController extends Controller {
	
	public function index()
	{
		return view('googleanalytics::index');
	}
	
}