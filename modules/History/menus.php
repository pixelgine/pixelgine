<?php

    $leftMenu  = Menu::instance('admin-menu');
    $rightMenu = Menu::instance('admin-menu-right');

    /**
     * @see https://github.com/pingpong-labs/menus
     *
     * @example adding additional menu.
     *
     * $leftMenu->url('your-url', 'The Title');
     *
     * $leftMenu->route('your-route', 'The Title');
     */
    $leftMenu->whereTitle(trans('modulemanager::modules.title'), function ($sub) {
        $sub->url(config('history.route.prefix', 'admin/history'), config('history.name'));
    }, 100, ['icon' => 'fa fa-puzzle-piece']);
