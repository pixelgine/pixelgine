<?php

Route::group(['prefix' => config('history.route.prefix', 'admin/history'), 'namespace' => 'Modules\History\Http\Controllers'], function()
{
	Route::get('/', 'HistoryController@index');
	Route::get('/db', 'DatabaseController@index');
	Route::get( '/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});
