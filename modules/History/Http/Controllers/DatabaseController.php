<?php namespace Modules\History\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Venturecraft\Revisionable\Revision;

class DatabaseController extends Controller {
	
	public function index()
	{
		$revisions = Revision::all();

		return view('history::admin.index', compact('revisions'));
	}
	
}