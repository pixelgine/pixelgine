<?php namespace Modules\History\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class HistoryController extends Controller {
	
	public function index()
	{
		return view('history::index');
	}
	
}