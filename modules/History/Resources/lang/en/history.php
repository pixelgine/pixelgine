<?php
return [
    'table' => [
        'user'  => 'User',
        'field' => 'Field',
        'oldValue' => 'Old value',
        'newValue' => 'New value',
        'revision_id' => 'Rev.',
        'revision_type' => 'Model',
        'created_at' => 'Date',
    ]
];