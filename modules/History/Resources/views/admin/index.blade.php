@extends($layout)

@section('content-header')
	<h1>
		{!! $title or 'All Revisions' !!} ({!! count($revisions) !!})
		{{--
		&middot;
		<small>{!! link_to_route('admin.modulemanager.create', trans('modulemanager::modules.create')) !!}</small>
		--}}
	</h1>
@stop

@section('content')

	@if(isset($search))
		@include('admin::users.search-form')
	@endif

	<table class="table">
		<thead>
			<th width="1"></th>
			<th>{{ trans('history::history.table.user') }}</th>
			<th>{{ trans('history::history.table.field') }}</th>
			<th width="20%">{{ trans('history::history.table.oldValue') }}</th>
			<th width="20%">{{ trans('history::history.table.newValue') }}</th>
			<th class="hidden-xs" width="1%">{{ trans('history::history.table.revision_type') }}</th>
			<th class="hidden-xs" width="1%">{{ trans('history::history.table.revision_id') }}</th>
			<th>{{ trans('history::history.table.created_at') }}</th>
		</thead>
		<tbody>
			@foreach ($revisions as $history)
			<tr>
				<td>
					@if( $history->key == "created_at" )
						<i class="fa fa-plus"></i>
					@elseif( $history->key == "deleted_at" )
						<i class="fa fa-trash-o"></i>
					@else
						<i class="fa fa-pencil"></i>
					@endif
				</td>
				<td>
					@if ( $history->userResponsible() )
						{{ $history->userResponsible()->name }}
					@endif
				</td>
				<td>
					{{ $history->fieldName() }}
				</td>
				<td>
					{{ $history->oldValue() }}
				</td>
				<td>
					{{ $history->newValue() }}
				</td>
				<td class="hidden-xs">{{ $history->revisionable_type }}</td>
				<td class="hidden-xs">{{ $history->revisionable_id }}</td>
				<td>{{ $history->created_at }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	<div class="text-center">

	</div>
@stop
