<?php namespace Modules\History\Providers;

use Illuminate\Support\ServiceProvider;

class HistoryServiceProvider extends ServiceProvider
{
	/**
	 * The class aliases array.
	 *
	 * @var array
	 */
	protected $aliases = [];

	/**
	 * The service provider classes array.
	 *
	 * @var array
	 */
	protected $providers = [
		'Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider',
	];

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		require __DIR__ . '/../composers.php';

		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
		$this->registerMenus();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerProviders();
		//$this->registerAliases();
	}

	/**
	 * Register service providers.
	 */
	public function registerProviders()
	{
		foreach ($this->providers as $provider) {
			$this->app->register($provider);
		}
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('history.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'history'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/history');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'history');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/history');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'history');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'history');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

	/**
	 * Register menus
	 *
	 * @return void
	 */
	public function registerMenus()
	{
		$menu = realpath(__DIR__ . '/../menus.php');

		if ( file_exists( $menu ) )
			include( $menu );
	}

}
