<?php

return [
	'name' => 'History',
	'route' => [
		'prefix' 	 => config('admin.prefix', 'admin') . '/history',
		'middleware' => config('admin.filter.auth'),
	],
];