<?php

Route::group(['prefix' => 'tracker', 'namespace' => 'Modules\Tracker\Http\Controllers'], function()
{
	Route::get('/', 'TrackerController@index');
});