<?php namespace Modules\Tracker\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class TrackerController extends Controller {
	
	public function index()
	{
		return view('tracker::index');
	}
	
}