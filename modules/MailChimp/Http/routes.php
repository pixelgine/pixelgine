<?php

Route::group(['prefix' => 'mailchimp', 'namespace' => 'Modules\MailChimp\Http\Controllers'], function()
{
	Route::get('/', 'MailChimpController@index');
});