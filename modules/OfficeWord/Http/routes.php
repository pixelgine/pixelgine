<?php

Route::group(['prefix' => 'officeword', 'namespace' => 'Modules\OfficeWord\Http\Controllers'], function()
{
	Route::get('/', 'OfficeWordController@index');
});