<?php namespace Modules\Officeword\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class OfficeWordController extends Controller {
	
	public function index()
	{
		return view('officeword::index');
	}
	
}