<?php namespace Modules\Officeword\Facades;

use Illuminate\Support\Facades\Facade;

class Word extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'word';
    }
}