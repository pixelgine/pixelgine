@extends($layout)

@section('content-header')
	<h1>
		{!! $title or 'Composer' !!}
		{{--
		&middot;
		<small>{!! link_to_route('admin.modulemanager.create', trans('modulemanager::modules.create')) !!}</small>
		--}}
	</h1>
@stop

@section('content')

	@if(isset($search))
		@include('admin::users.search-form')
	@endif

	<div class="text-center">

		<a href="{{ URL::route('admin.development.composer.update') }}" class="btn btn-primary btn-composer-ajax">
			Update
		</a>

		<a href="{{ URL::route('admin.development.composer.install') }}" class="btn btn-primary btn-composer-ajax">
			Install
		</a>

		<a href="{{ URL::route('admin.development.composer.dump.autoload') }}" class="btn btn-primary btn-composer-ajax">
			Dump-autoload
		</a>
	</div>

	<textarea style="width: 100%;" rows="30" id="composer-output"></textarea>
@stop

@section('script')
	@parent

	<script>
		$(document).ready(function(){
			$(document).on('click', '.btn-composer-ajax', function(e)
			{
				e.preventDefault();

				var link = $(this).attr('href');
				alert(link);
				$.get( link , function(data)
				{
					if ( data )
					{
						$("#composer-output").val(data);
					}
				});

				return false;
			});

		});
	</script>
@stop