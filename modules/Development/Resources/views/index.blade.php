@extends($layout)

@section('content-header')
	<h1>
		{!! $title or 'Composer' !!}
		{{--
		&middot;
		<small>{!! link_to_route('admin.modulemanager.create', trans('modulemanager::modules.create')) !!}</small>
		--}}
	</h1>
@stop


@section('content')
	
	<h1>Hello World</h1>
	
	<p>
		This view is loaded from module: {!! config('development.name') !!}
	</p>

@stop