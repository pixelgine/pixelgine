@extends($layout)

@section('content-header')
	<h1>
		{!! $title or 'Artisan' !!}
		{{--
		&middot;
		<small>{!! link_to_route('admin.modulemanager.create', trans('modulemanager::modules.create')) !!}</small>
		--}}
	</h1>
@stop

@section('content')

	@if(isset($search))
		@include('admin::users.search-form')
	@endif

	<div class="text-center">

	</div>
@stop
