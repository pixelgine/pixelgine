<?php
return [
    'tools' => [
        'dbhelper' => 'DB Helper',
        'artisan'   => 'Artisan',
        'composer' => 'Composer'
    ]
];