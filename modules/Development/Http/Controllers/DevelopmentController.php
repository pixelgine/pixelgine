<?php namespace Modules\Development\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class DevelopmentController extends Controller {
	
	public function index()
	{
		return view('development::index');
	}

	public function artisan()
	{
		return view('development::artisan');
	}

	public function composer()
	{
		return view('development::composer');
	}

}