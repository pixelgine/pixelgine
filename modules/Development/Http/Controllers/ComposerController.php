<?php namespace Modules\Development\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use \Symfony\Component\Console\Input\StringInput;
use \Symfony\Component\Console\Output\StreamOutput;
use \Composer\Console\Application AS Composer;

class ComposerController extends Controller
{

	public function __construct()
	{
		$this->init();
	}

	private function init()
	{
		set_time_limit(-1);
		ini_set('memory_limit', '256M');
		putenv('COMPOSER_HOME=' . __DIR__ . '/extracted/bin/composer');
	}

	public function index()
	{
		return view('development::index');
	}

	public function update()
	{
		$this->command('update');
	}

/*	public function updateSelf()
	{
		$this->command('install');
	}*/

	public function install()
	{
		$this->command('install');
		//`echo '1' | composer install --no-interaction`;
	}

	public function dumpAutoload()
	{
		$this->command('dump-autoload');
	}

	public function command( $command, $params = [] )
	{
		$path = str_replace('\\', '\\\\', base_path());

		//$input  = new StringInput( $command . ' -vvv -d \'' . $path . '\'' );
		$input  = new StringInput( $command . ' -d \'' . $path . '\'' );
		$output = new StreamOutput( fopen('php://output', 'w') );

		$app = new Composer();
		$app->run($input,$output);
	}
	//https://github.com/composer/composer/issues/1906
	//https://github.com/CurosMJ/NoConsoleComposer

}