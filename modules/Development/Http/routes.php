<?php

Route::group(['prefix' => config('admin.prefix', 'admin'), 'namespace' => 'Modules\Development\Http\Controllers'], function()
{
	Route::group(['middleware' => config('admin.filter.auth')], function ()
	{


		Route::group(['prefix' => 'development'], function()
		{
			Route::get('/', 'DevelopmentController@index');
			Route::get('/artisan',   ['as' => 'admin.development.artisan',  'uses' => 'DevelopmentController@artisan']);
			Route::get('/composer',  ['as' => 'admin.development.composer', 'uses' => 'DevelopmentController@composer']);
			/*Route::get('/composer/self-update',  [
				'as' => 'admin.development.composer.update.self',
				'uses' => 'ComposerController@updateSelf'
			]);*/
			Route::get('/composer/update',  [
				'as' => 'admin.development.composer.update',
				'uses' => 'ComposerController@update'
			]);
			Route::get('/composer/install',  [
				'as' => 'admin.development.composer.install',
				'uses' => 'ComposerController@update'
			]);
			Route::get('/composer/dump-autoload',  [
				'as' => 'admin.development.composer.dump.autoload',
				'uses' => 'ComposerController@update'
			]);
		});

		//Route::get('/', ['as' => 'admin.modulemanager', 'uses' => 'ModulesController@index']);

		/*Route::resource('modules', 'ModuleManagerController', [
			'except' => 'show',
			'names' => [
				'index' 	=> 'admin.modulemanager.index',
				'create' 	=> 'admin.modulemanager.create',
				'store' 	=> 'admin.modulemanager.store',
				'show' 		=> 'admin.modulemanager.show',
				'update' 	=> 'admin.modulemanager.update',
				'edit' 		=> 'admin.modulemanager.edit',
				'destroy' 	=> 'admin.modulemanager.destroy'
			],
		]);
		Route::get('modules/{name}/enable',  ['as' => 'admin.modulemanager.enable',  'uses' => 'ModuleManagerController@enable']);
		Route::get('modules/{name}/disable', ['as' => 'admin.modulemanager.disable', 'uses' => 'ModuleManagerController@disable']);
		Route::get('modules/{name}/install', ['as' => 'admin.modulemanager.install', 'uses' => 'ModuleManagerController@install']);
		Route::get('modules/{name}/destroy', ['as' => 'admin.modulemanager.destroy', 'uses' => 'ModuleManagerController@destroy']);
		Route::get('modules/{name}/migrate', ['as' => 'admin.modulemanager.migrate', 'uses' => 'ArtisanCommandController@migrate']);
		Route::get('modules/{name}/seed', 	 ['as' => 'admin.modulemanager.seed', 	 'uses' => 'ArtisanCommandController@seed']);
		Route::get('modules/{name}/publish', ['as' => 'admin.modulemanager.publish', 'uses' => 'ArtisanCommandController@publish']);
		*/
	});
});
