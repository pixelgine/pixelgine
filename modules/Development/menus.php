<?php

    $leftMenu  = Menu::instance('admin-menu');
    $rightMenu = Menu::instance('admin-menu-right');

    /**
     * @see https://github.com/pingpong-labs/menus
     *
     * @example adding additional menu.
     *
     * $leftMenu->url('your-url', 'The Title');
     *
     * $leftMenu->route('your-route', 'The Title');
     */
    //$leftMenu->enableOrdering();
    //$leftMenu->route('admin.eav.index', trans('eav::models.title'), [], 100, ['icon' => 'fa fa-puzzle-piece']);

    $leftMenu->dropdown(config('development.name'), function ($sub) {
        $sub->route('admin.development.artisan', trans('development::config.tools.artisan'), []);
        $sub->route('admin.development.composer', trans('development::config.tools.composer'), []);
        $sub->url(URL::to('/dbhelper'),  trans('development::config.tools.dbhelper'),    []);
        $sub->url(URL::to('/db2migrate'),  trans('development::config.tools.db2migrate'),    []);
    }, 0, ['icon' => 'fa fa-code']);
