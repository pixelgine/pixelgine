<?php namespace Modules\Cache\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class CacheController extends Controller {
	
	public function index()
	{
		return view('cache::index');
	}
	
}