<?php namespace Modules\Akismet\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class AkismetController extends Controller {
	
	public function index()
	{
		return view('akismet::index');
	}
	
}