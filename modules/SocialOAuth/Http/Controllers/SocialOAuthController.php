<?php namespace Modules\Socialoauth\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class SocialOAuthController extends Controller {
	
	public function index()
	{
		return view('socialoauth::index');
	}
	
}