<?php

Route::group(['prefix' => 'socialoauth', 'namespace' => 'Modules\SocialOAuth\Http\Controllers'], function()
{
	Route::get('/', 'SocialOAuthController@index');
});