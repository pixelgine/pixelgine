<?php namespace Modules\Webshop\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class WebShopController extends Controller {
	
	public function index()
	{
		return view('webshop::index');
	}
	
}