<?php

Route::group(['prefix' => 'webshop', 'namespace' => 'Modules\WebShop\Http\Controllers'], function()
{
	Route::get('/', 'WebShopController@index');
});