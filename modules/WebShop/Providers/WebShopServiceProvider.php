<?php namespace Modules\Webshop\Providers;

use Illuminate\Support\ServiceProvider;

class WebShopServiceProvider extends ServiceProvider
{
	/**
	 * The class aliases array.
	 *
	 * @var array
	 */
	protected $aliases = [];

	/**
	 * The service provider classes array.
	 *
	 * @var array
	 */
	protected $providers = [
		'Aimeos\Shop\ShopServiceProvider',
	];

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{		
		//
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('webshop.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'webshop'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/webshop');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'webshop');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/webshop');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'webshop');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'webshop');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
