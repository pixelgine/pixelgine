<?php namespace Modules\Modulegenerator\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class ModuleGeneratorController extends Controller {
	
	public function index()
	{
		return view('modulegenerator::index');
	}
	
}