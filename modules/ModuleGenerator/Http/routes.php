<?php

Route::group(['prefix' => 'modulegenerator', 'namespace' => 'Modules\ModuleGenerator\Http\Controllers'], function()
{
	Route::get('/', 'ModuleGeneratorController@index');
});