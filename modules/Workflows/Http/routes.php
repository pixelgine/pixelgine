<?php

Route::group(['prefix' => 'workflows', 'namespace' => 'Modules\Workflows\Http\Controllers'], function()
{
	Route::get('/', 'WorkflowsController@index');
});