<?php

Route::group(['prefix' => 'gallery', 'namespace' => 'Modules\Gallery\Http\Controllers'], function()
{
	Route::get('/', 'GalleryController@index');
});