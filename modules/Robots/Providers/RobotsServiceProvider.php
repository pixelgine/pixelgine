<?php namespace Modules\Robots\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class RobotsServiceProvider extends ServiceProvider
{
	/**
	 * The class aliases array.
	 *
	 * @var array
	 */
	protected $aliases = [
		'Robots' 	=> 'EllisTheDev\Robots\RobotsFacade',
		'Honeypot'	=> 'Msurguy\Honeypot\HoneypotFacade',
		'Recaptcha' => 'Greggilbert\Recaptcha\Facades\Recaptcha',
	];

	/**
	 * The service provider classes array.
	 *
	 * @var array
	 */
	protected $providers = [
		'EllisTheDev\Robots\RobotsServiceProvider',
		'Msurguy\Honeypot\HoneypotServiceProvider',
		'Greggilbert\Recaptcha\RecaptchaServiceProvider',
	];

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerProviders();
		$this->registerAliases();
	}

	/**
	 * Register service providers.
	 */
	public function registerProviders()
	{
		foreach ($this->providers as $provider) {
			$this->app->register($provider);
		}
	}

	/**
	 * Register class aliases.
	 */
	public function registerAliases()
	{
		AliasLoader::getInstance($this->aliases)->register();
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('robots.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'robots'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/robots');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'robots');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/robots');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'robots');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'robots');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
