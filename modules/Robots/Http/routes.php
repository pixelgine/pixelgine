<?php
/*
Route::group(['prefix' => 'robots', 'namespace' => 'Modules\Robots\Http\Controllers'], function()
{
	Route::get('/', 'RobotsController@index');
});
*/

Route::get('/robots.txt', function()
{
	if (App::environment() != 'production')
	{
		Robots::addUserAgent('*');
		Robots::addSitemap('sitemap.xml');

		foreach( [
			 '/admin',
			 '/administer',
			 '/administration',
			 '/backend',
			 '/cms',
			 '/control_panel',
			 '/customers',
			 '/files',
			 '/login',
			 '/restricted',
		 ] AS $url )
			Robots::addDisallow( $url );

	}
	else
	{
		Robots::addDisallow('*');
	}

	return Response::make(Robots::generate(), 200, ['Content-Type' => 'text/plain']);
});