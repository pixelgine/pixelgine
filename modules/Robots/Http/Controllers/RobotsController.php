<?php namespace Modules\Robots\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RobotsController extends Controller {
	
	public function index()
	{
		return view('robots::index');
	}
	
}