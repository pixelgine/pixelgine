<?php namespace Modules\Dropbox\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class DropboxController extends Controller {
	
	public function index()
	{
		return view('dropbox::index');
	}
	
}