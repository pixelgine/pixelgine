<?php

Route::group(['prefix' => 'polls', 'namespace' => 'Modules\Polls\Http\Controllers'], function()
{
	Route::get('/', 'PollsController@index');
});