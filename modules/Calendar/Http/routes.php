<?php

Route::group(['prefix' => 'calendar', 'namespace' => 'Modules\Calendar\Http\Controllers'], function()
{
	Route::get('/', 'CalendarController@index');
});