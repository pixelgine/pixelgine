<?php namespace Modules\Officeexcel\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class OfficeExcelController extends Controller {
	
	public function index()
	{
		return view('officeexcel::index');
	}
	
}