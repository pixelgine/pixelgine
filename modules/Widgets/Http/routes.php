<?php

Route::group(['prefix' => 'widgets', 'namespace' => 'Modules\Widgets\Http\Controllers'], function()
{
	Route::get('/', 'WidgetsController@index');
});