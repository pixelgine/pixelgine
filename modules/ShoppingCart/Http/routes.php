<?php

Route::group(['prefix' => 'shoppingcart', 'namespace' => 'Modules\ShoppingCart\Http\Controllers'], function()
{
	Route::get('/', 'ShoppingCartController@index');
});