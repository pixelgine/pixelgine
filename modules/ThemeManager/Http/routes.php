<?php

Route::group(['prefix' => 'thememanager', 'namespace' => 'Modules\ThemeManager\Http\Controllers'], function()
{
	Route::get('/', 'ThemeManagerController@index');
});