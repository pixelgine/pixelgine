<?php namespace Modules\Thememanager\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class ThemeManagerController extends Controller {
	
	public function index()
	{
		return view('thememanager::index');
	}
	
}