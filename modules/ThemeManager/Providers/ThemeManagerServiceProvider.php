<?php namespace Modules\Thememanager\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class ThemeManagerServiceProvider extends ServiceProvider
{
	/**
	 * The class aliases array.
	 *
	 * @var array
	 */
	protected $aliases = [
		//'Theme' => 'Pingpong\Themes\ThemeFacade',
		'Theme' => 'Modules\Thememanager\Facades\ThemeFacade',
	];

	/**
	 * The service provider classes array.
	 *
	 * @var array
	 */
	protected $providers = [
		'Pingpong\Themes\ThemesServiceProvider',
	];

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
		$this->registerThemeViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerProviders();
		$this->registerAliases();
	}

	/**
	 * Register service providers.
	 */
	public function registerProviders()
	{
		foreach ($this->providers as $provider) {
			$this->app->register($provider);
		}
	}

	/**
	 * Register class aliases.
	 */
	public function registerAliases()
	{
		AliasLoader::getInstance($this->aliases)->register();
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('thememanager.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'thememanager'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/thememanager');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'thememanager');
	}

	/**
	 * Register themes views.
	 *
	 * @return void
	 */
	public function registerThemeViews()
	{
		$viewPath = realpath( \Theme::getPath() . '/' . \Theme::getCurrent() . '/views' );
		\View::addLocation($viewPath);
		\View::addLocation(base_path('resources/views'));
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/thememanager');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'thememanager');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'thememanager');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
