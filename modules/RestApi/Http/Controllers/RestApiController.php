<?php namespace Modules\Restapi\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RestApiController extends Controller {
	
	public function index()
	{
		return view('restapi::index');
	}
	
}