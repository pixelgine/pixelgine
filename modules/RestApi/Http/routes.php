<?php

Route::group(['prefix' => 'restapi', 'namespace' => 'Modules\RestApi\Http\Controllers'], function()
{
	Route::get('/', 'RestApiController@index');
});