<?php

Route::group(['prefix' => 'translations', 'namespace' => 'Modules\Translations\Http\Controllers'], function()
{
	Route::get('/', 'TranslationsController@index');
});