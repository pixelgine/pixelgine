<?php namespace Modules\Translations\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class TranslationsController extends Controller {
	
	public function index()
	{
		return view('translations::index');
	}
	
}