<?php

    $leftMenu  = Menu::instance('admin-menu');
    $rightMenu = Menu::instance('admin-menu-right');

    /**
     * @see https://github.com/pingpong-labs/menus
     *
     * @example adding additional menu.
     *
     * $leftMenu->url('your-url', 'The Title');
     *
     * $leftMenu->route('your-route', 'The Title');
     */
$leftMenu->whereTitle(trans('modulemanager::modules.title'), function ($sub) {
    //$sub->header('ACCOUNT');
    $sub->url(config('translation-manager.route.prefix', 'admin/translations'), config('translations.name'));
    //$sub->route('admin.modulemanager.create', trans('modulemanager::modules.create'), [], 2);
}, 100, ['icon' => 'fa fa-puzzle-piece']);
